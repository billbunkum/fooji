#fooji project
+ used to apply for fooji developer job

## bitbucket
+ repository: fooji
+ private repo

## local setup
+ `$ workon fooji`
+ `$ pip install -r requirements.txt`
+ reference fooji-proj/README.md for Brunch instructions and build file structures
+ setup brunch-config.coffee w/ proper paths to concat css & jss + utilize sass plugins 
`module.exports = 
  files:
    javascripts:
      joinTo:
        "app.js": /^app/, "vendor.js": /^(?!app)/
    stylesheets:
      joinTo:
        "app.css"
  plugins:
    sass:
      mode: "native"
      options:
        includePaths: "node_modules/foundation/scss"`

## working the env
+ `$ brunch watch --server`
+ `$ coffee watch <filename>`
+ `$ compass watch` -> must do this within the compass `dir`

## Site should do the following
+ be responsive
+ "You clicked it!" dialog appears when "Get Started with Twitter" button is clicked
+ utilise the Google Font - Muli
+ use Brunch as a build tool
+ use NPM to designate dependencies
+ use SCSS/SASS instead of CSS - .css files are organized
+ use CoffeeScript instead of vanilla Javascript - .coffee files are organized
+ use CSS transitions if time allows