module.exports = 
  files:
    javascripts:
      joinTo:
        "app.js": /^app/, "vendor.js": /^(?!app)/
    stylesheets:
      joinTo:
        "app.css"
  modules:
    autoRequire:
      "javascripts/app.js": ['clicker']
  plugins:
    sass:
      mode: "native"
      options:
        includePaths: "node_modules/foundation/scss"