exports =
  files:
    javascripts: {
      joinTo: 'app.js': /^app/,
      joinTo: 'vendor.js': /^(?!app)/
    }
    stylesheets: {joinTo: 'app.css'}

  plugins:
    babel: {presets: ['lastest']}
    sass:
      mode: 'native'
      options:
        includePaths:
          'node_modules/sass-brunch'